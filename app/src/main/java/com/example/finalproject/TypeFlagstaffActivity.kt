package com.example.finalproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class TypeFlagstaffActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_type_listofflag)

        val button = findViewById<Button>(R.id.btnAfrica)
        button.setOnClickListener {
            val intent = Intent(this, TypeAfricaActivity::class.java)
            startActivity(intent)
        }
    }
}