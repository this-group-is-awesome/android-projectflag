package com.example.finalproject

class Constants {

    fun getQuestions(): ArrayList<Question>{
        val questionsList = ArrayList<Question>()
        val que1 = Question(
            1,
            "What country does this flag belong to?",
            R.drawable.andorra,
            optionOne = "Myanmar",
            optionTwo = "Iceland",
            optionThree = "Andorra",
            optionFour = "Thailand",
            correctAnswer = 3
        )
        questionsList.add(que1)

        val que2 = Question(
            1,
            "What country does this flag belong to?",
            R.drawable.japan,
            optionOne = "Japan",
            optionTwo = "Hong Kong",
            optionThree = "Vietnam",
            optionFour = "India",
            correctAnswer = 1
        )
        questionsList.add(que2)


        val que3 = Question(
            1,
            "What country does this flag belong to?",
            R.drawable.bhutan,
            optionOne = "Bolivia",
            optionTwo = "Iceland",
            optionThree = "Nauru",
            optionFour = "Bhutan",
            correctAnswer = 4
        )
        questionsList.add(que3)

        val que4 = Question(
            1,
            "What country does this flag belong to?",
            R.drawable.denmark,
            optionOne = "Belarus",
            optionTwo = "Albania",
            optionThree = "Denmark",
            optionFour = "Brunei",
            correctAnswer = 3
        )
        questionsList.add(que4)


        val que5 = Question(
            1,
            "What country does this flag belong to?",
            R.drawable.estonia,
            optionOne = "Qatar",
            optionTwo = "Estonia",
            optionThree = "Cuba",
            optionFour = "Argentina",
            correctAnswer = 2
        )
        questionsList.add(que5)

        val que6 = Question(
            1,
            "What country does this flag belong to?",
            R.drawable.austria,
            optionOne = "Uruguay",
            optionTwo = "Panama",
            optionThree = "Peru",
            optionFour = "Austria",
            correctAnswer = 4
        )
        questionsList.add(que6)

        val que7 = Question(
            1,
            "What country does this flag belong to?",
            R.drawable.iran,
            optionOne = "Iran",
            optionTwo = "Iraq",
            optionThree = "Israel",
            optionFour = "Indonesia",
            correctAnswer = 1
        )
        questionsList.add(que7)


        val que8 = Question(
            1,
            "What country does this flag belong to?",
            R.drawable.cyprus,
            optionOne = "Ghana",
            optionTwo = "Gabon",
            optionThree = "Cyprus",
            optionFour = "Eswatini",
            correctAnswer = 3
        )
        questionsList.add(que8)


        val que9 = Question(
            1,
            "What country does this flag belong to?",
            R.drawable.egypt,
            optionOne = "Latvia",
            optionTwo = "Egypt",
            optionThree = "Kosovo",
            optionFour = "Finland",
            correctAnswer = 2
        )
        questionsList.add(que9)


        val que10 = Question(
            1,
            "What country does this flag belong to?",
            R.drawable.hungary,
            optionOne = "Romania",
            optionTwo = "Ukraine",
            optionThree = "Hungary",
            optionFour = "Sweden",
            correctAnswer = 3
        )
        questionsList.add(que10)

        return questionsList
    }
}